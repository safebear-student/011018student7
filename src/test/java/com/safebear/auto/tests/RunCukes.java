package com.safebear.auto.tests;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterClass;

import java.io.File;

@CucumberOptions(
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber/extent_report.html"},
//        plugin = {"pretty","html:target/cucumber"},
        tags = "~@todo",
        glue = "com.safebear.auto.tests",
        features = "classpath:toolslist.features/login.feature"
)
public class RunCukes extends AbstractTestNGCucumberTests{

    @AfterClass
    public static void tearDown() {
        Reporter.loadXMLConfig(new File("src/test/resources/reports/extent-config.xml"));
        Reporter.setSystemInfo("user", System.getProperty("user.name"));
        Reporter.setSystemInfo("os", "Windows");
        Reporter.setTestRunnerOutput("Sample test runner output message");
    }

}
