package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LoginPageLocators {
    private By usernamefield = By.id("username");
    private By passwordfield = By.id("password");
    private By submitbutton= By.id("enter");
    private By validationError = By.xpath(".//p[@id='rejectLogin']/b");
}
